<?php
// Aside menu
return [

    'items' => [
        // Countries index
        [
            'title' => 'Countries',
            'root' => true,
            'icon' => 'media/svg/icons/Map/position.svg', 
            'page' => '/countries',
            'new-tab' => false,
        ],
        [
            'title' => 'Catrgories',
            'root' => true,
            'icon' => 'media/svg/icons/Design/Layers.svg', 
            'page' => '/categories',
            'new-tab' => false,
        ], 
        [
            'title' => 'Items',
            'root' => true,
            'icon' => 'media/svg/icons/General/Star.svg',
            'page' => '/items',
            'new-tab' => false,
        ], 
        [
            'title' => 'Stores',
            'root' => true,
            'icon' => 'media/svg/icons/Shopping/cart1.svg',
            'page' => '/stores',
            'new-tab' => false,
        ],    

    ]

];
