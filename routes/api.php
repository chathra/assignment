<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Api\CategoryController;
use App\Http\Controllers\Api\SubCategoryController;
use App\Http\Controllers\Api\StoreController;
use App\Http\Controllers\Api\CityController;
use App\Http\Controllers\Api\AreaController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Guest routes
Route::post('/register', [App\Http\Controllers\API\AuthController::class, 'register']);
Route::post('/login', [App\Http\Controllers\API\AuthController::class, 'login']);
//Authencation required routes
Route::group(['middleware' => []], function () {
    Route::get('/profile', function(Request $request) {
        return auth()->user();
    });
    Route::post('/logout', [App\Http\Controllers\API\AuthController::class, 'logout']);
    
    Route::group(['prefix'=>'categories','as'=>'categories.api.','controller'=>CategoryController::class], function(){
        Route::get('/', 'index')->name('index')->middleware('language.check');
        Route::get('/{id}', 'show')->name('show')->middleware('language.check');
        Route::post('/', 'store')->name('store');
    });

    Route::group(['prefix'=>'sub-categories','as'=>'sub-categories.api.','controller'=>SubCategoryController::class], function(){
        Route::get('/', 'index')->name('index')->middleware('language.check');
        
    });

    Route::get('categories/{categoryId}/sub-categories', [SubCategoryController::class,'selectByCategory'])->middleware('language.check');

    Route::get('stores', [StoreController::class,'index'])->middleware('language.check');

    Route::get('/cities', [CityController::class,'index'])->middleware('language.check');

    Route::get('/areas', [AreaController::class,'index'])->middleware('language.check');

});
