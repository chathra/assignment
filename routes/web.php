<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\CountryController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\SubCategoryController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\StoreController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('countries.index');
});
//Route::get('countries','CountryController@index');
//Countries routes
Route::group(['prefix'=>'countries','as'=>'countries.','controller'=>CountryController::class], function(){
    Route::get('/', 'index')->name('index');
    Route::get('/create', 'create')->name('create');
    Route::get('/{country}', 'show')->name('show');
    Route::get('/edit/{country}', 'edit')->name('edit');
    Route::delete('/{country}', 'destroy')->name('delete');
    Route::put('/{country}', 'update')->name('update');
    Route::post('/', 'store')->name('store');

});

Route::group(['prefix'=>'categories','as'=>'categories.','controller'=>CategoryController::class], function(){
    Route::get('/', 'index')->name('index');
    Route::get('/create', 'create')->name('create');
    Route::get('/{id}', 'show')->name('show');
    Route::get('/edit/{category}', 'edit')->name('edit');
    Route::delete('/{category}', 'destroy')->name('delete');
    Route::put('/{category}', 'update')->name('update');
    Route::post('/', 'store')->name('store');

    Route::group(['prefix'=>'{categoryId}/sub-categories','as'=>'sub-categories.','controller'=>SubCategoryController::class], function(){
        Route::get('/', 'index')->name('index');
        Route::get('/create', 'create')->name('create');
        Route::get('/{subCategoryId}', 'show')->name('show');
        Route::delete('/{subCategoryId}', 'destroy')->name('delete');
        Route::put('/{subCategoryId}', 'update')->name('update');
        Route::post('/', 'store')->name('store');
    
    });

});

Route::group(['prefix'=>'items','as'=>'items.','controller'=>ItemController::class], function(){
    Route::get('/', 'index')->name('index');
    Route::get('/create', 'create')->name('create');
    Route::get('/{id}', 'show')->name('show');
    Route::delete('/{id}', 'destroy')->name('delete');
    Route::put('/{id}', 'update')->name('update');
    Route::post('/', 'store')->name('store');
    Route::get('/edit/{id}', 'edit')->name('edit');

});

Route::group(['prefix'=>'stores','as'=>'stores.','controller'=>StoreController::class], function(){
    Route::get('/', 'index')->name('index');
    Route::get('/create', 'create')->name('create');
    Route::get('/{id}', 'show')->name('show');
    Route::delete('/{id}', 'destroy')->name('delete');
    Route::put('/{id}', 'update')->name('update');
    Route::post('/', 'store')->name('store');
    Route::get('/edit/{id}', 'edit')->name('edit');

});



Route::get('/login', function () {
    return view('auth.login');
});