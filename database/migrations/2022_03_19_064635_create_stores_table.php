<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stores', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('title_author_id');
            $table->unsignedBigInteger('store_location_text_author_id');
            $table->string('phone',20);
            $table->decimal('latitude', 10, 8)->nullable();
            $table->decimal('longitude', 11, 8)->nullable();
            $table->string('image_url')->nullable();
            $table->string('cover_image_url')->nullable();
            $table->string('google_maps_link')->nullable();
            $table->unsignedBigInteger('slogan_author_id')->nullable();
            $table->boolean('is_active')->default(false);
            $table->boolean('hot_price_adding_allowed')->default(false);
            $table->timestamps();

            $table->foreign('title_author_id')
                ->references('id')->on('users')
                ->cascadeOnDelete();

            $table->foreign('store_location_text_author_id')
                ->references('id')->on('users')
                ->cascadeOnDelete();

            $table->foreign('slogan_author_id')
                ->references('id')->on('users')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stores');
    }
}
