<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreTranslations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('store_id');
            $table->unsignedMediumInteger('language_id');
            $table->string('title');
            $table->string('location_text');
            $table->string('slogan')->nullable();
            $table->timestamps();

            $table->foreign('store_id')
                ->references('id')->on('stores')
                ->cascadeOnDelete();

            $table->foreign('language_id')
                ->references('id')->on('languages')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store_translations');
    }
}
