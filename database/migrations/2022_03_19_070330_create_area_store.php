<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAreaStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('area_store', function (Blueprint $table) {
            $table->unsignedSmallInteger('area_id');
            $table->unsignedBigInteger('store_id');

            $table->foreign('area_id')
                ->references('id')->on('areas')
                ->cascadeOnDelete();

            $table->foreign('store_id')
                ->references('id')->on('stores')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('area_store');
    }
}
