<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSubCategoryTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sub_category_translations', function (Blueprint $table) {
            $table->id();
            $table->unsignedSmallInteger('sub_category_id');
            $table->unsignedMediumInteger('language_id');
            $table->string('title');
            $table->timestamps();

            $table->foreign('sub_category_id')
                ->references('id')->on('sub_categories')
                ->cascadeOnDelete();

            $table->foreign('language_id')
                ->references('id')->on('languages')
                ->cascadeOnDelete();

            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sub_category_translations');
    }
}
