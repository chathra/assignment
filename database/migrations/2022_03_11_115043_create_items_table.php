<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('items', function (Blueprint $table) {
            $table->id();
            $table->unsignedSmallInteger('category_id');
            $table->unsignedSmallInteger('sub_category_id');
            $table->unsignedBigInteger('title_author_id');
            $table->unsignedBigInteger('des_author_id');
            $table->double('price',15,2);
            $table->double('new_price',15,2)->nullable();
            $table->text('arrival_range');
            $table->unsignedInteger('in_stock');
            $table->timestamps();

            $table->foreign('category_id')
                ->references('id')->on('categories')
                ->cascadeOnDelete();

            $table->foreign('sub_category_id')
                ->references('id')->on('sub_categories')
                ->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('items');
    }
}
