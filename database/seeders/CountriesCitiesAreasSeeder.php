<?php

namespace Database\Seeders;

use App\Models\Country;
use App\Models\Language;
use Illuminate\Database\Seeder;
use App\Models\CountryTranslation;
use App\Models\City;
use App\Models\CityTranslation;
use App\Models\Area;
use App\Models\AreaTranslation;


class CountriesCitiesAreasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $baseUrl="http://46.101.235.217/api/";
        $countriesUrl=$baseUrl."/countries";

        \DB::beginTransaction();

        try{

            $englishLanguageId=Language::where('iso','en')->first()->id;
            $arabicLanguageId=Language::where('iso','ar')->first()->id;

            $countries = json_decode(file_get_contents($countriesUrl), true);
            
            foreach($countries as $countryArray){
                $country=Country::create([
                    'flag_url'=>$baseUrl.$countryArray['flag']['url'],
                ]);

                CountryTranslation::create([
                    'country_id'=>$country->id,
                    'language_id'=>$englishLanguageId,
                    'name'=>$countryArray['name_en']
                ]);

                CountryTranslation::create([
                    'country_id'=>$country->id,
                    'language_id'=>$arabicLanguageId,
                    'name'=>$countryArray['name_local']
                ]);

                foreach($countryArray['cities'] as $cityArray){              
                    $city=City::create([
                        'country_id'=>$country->id                  
                    ]);

                    CityTranslation::create([
                        'city_id'=>$city->id,
                        'language_id'=>$englishLanguageId,
                        'name'=>$cityArray['name_en']

                    ]);

                    CityTranslation::create([
                        'city_id'=>$city->id,
                        'language_id'=>$arabicLanguageId,
                        'name'=>$cityArray['name_local']

                    ]);

                    $areasGetUrl=$baseUrl."/areas/by-country/".$countryArray['id'];
                    $areas = json_decode(file_get_contents($areasGetUrl), true);

                    $areasOfCity=collect($areas)->where('city.id',$cityArray['id']);

                    foreach($areasOfCity as $areaArray){
                        $area=Area::create([
                            'city_id'=>$city->id
                        ]);

                        AreaTranslation::create([
                            'area_id'=>$area->id,
                            'language_id'=>$englishLanguageId,
                            'name'=>$areaArray['name_en']
                        ]);

                        AreaTranslation::create([
                            'area_id'=>$area->id,
                            'language_id'=>$arabicLanguageId,
                            'name'=>$areaArray['name_local']
                        ]);
                    }
                } 
            }

            \DB::commit();
        }catch(\Exception $e){
            \DB::rollback();
            dd($e);
        }
           


            // $citiesArray=[];
            // $areasArray=[];
            // $url="http://46.101.235.217/api/areas/by-country/".$country['id'];
            // $areas = collect(json_decode(file_get_contents($url), true));
            // foreach($country['cities'] as $city){
              
            //     $cityDB=City::create([
            //         'country_id'=>$CountryDB->id,
            //         'name_en'=>$city['name_en'],
            //         'name_local'=>$city['name_local']                   
            //     ]);

            //     $areasOfCity=$areas->where('city.id',$city['id']);
                
            //     foreach($areasOfCity as $area){
            //         $tempAreaArray=[];
            //         $tempAreaArray['name']=$area['name_en'];
            //         $tempAreaArray['name_local']=$area['name_local'];
            //         $tempAreaArray['city_id']=$cityDB->id;

            //         array_push($areasArray,$tempAreaArray);
            //     }
                
            //     Area::insert($areasArray);
            // }
        }
    }

