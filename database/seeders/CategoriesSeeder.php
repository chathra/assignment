<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class CategoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=\App\Models\User::Create([
            'name'=>'Jane Doe',
            'email'=>'janedoe@yopmail.com',
            'password'=>Hash::make('12345678')
        ]);

        $category=\App\Models\Category::create([
            'title_author_id'=> $user->id,
            'image_url'=>'/sample/image/url.jpg'
        ]);

        $data=[
                [
                    'category_id'=>$category->id,
                    'language_id'=>1,
                    'title'=>'Fruit and Vagitables'
                ],
                [
                    'category_id'=>$category->id,
                    'language_id'=>2,
                    'title'=>'الفاكهة والخضروات'
                ]
            ];

        \App\Models\CategoryTranslation::insert($data);
    }
}
