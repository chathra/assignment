<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user=\App\Models\User::create([
            'name'=>'John Doe',
            'email'=>'johndoe@yopmail.com',
            'password'=>Hash::make('12345678')
        ]);

        $token = $user->createToken('auth_token')->plainTextToken;

        $this->command->alert('Please use this Bearer token for API requests -> '.$token);
    }
}
