# App setup instructions

##### 1. Clone the repo
##### 2.Install dependancies by <code> composer install </code> and <code> npm install</code>
##### 3.Copy .env.sample file as .env
##### 4.Generate app encription key by <code>php artisan key:generate </code>
##### 5.Link storage by <code>php artisan storage:link </code>
##### 6.Configure database details in the .env
##### 7.Run <code>php artisan migrate --seed</code> to create end seed the database , note- copy the Bearer token for API requests
##### 8.Run <code>php artisan serve </code> and go to the showing address 


# API details

#### Use the Bearer token copied while database seeding for each request 
#### get all categories - >GET /api/categories?language=en
#### get one category - >GET /api/categories/1/?language=en
#### save one category - >POST /api/categories/