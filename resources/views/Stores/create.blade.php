{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom example example-compact">
    <div class="card-header">
        <h3 class="card-title">
            Create New Store
        </h3>
        <div class="card-toolbar">
            <div class="example-tools justify-content-center">
                <a href="{{ route('stores.index') }}" class="btn btn-secondary">Go Back</a>
            </div>
        </div>
    </div>
    <form action="{{ route('stores.store') }}" id="store-update-form" method="POST" enctype="multipart/form-data">
        {{csrf_field()}}
         <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Title English<span class="text-danger">*</span></label>
                    <input required type="text" name="title_en" value="{{ old('title_en') }}"  class="form-control" placeholder="Enter Title"/>
                </div>
                <div class="col-lg-6">
                    <label>Title Arabic<span class="text-danger">*</span></label>
                    <input required  type="text" name="title_ar" value="{{ old('title_ar') }}"  class="form-control" placeholder="Enter Title"/>
                </div>
            </div>
            <div class="form-group row">                
                <div class="col-lg-6">
                    <label>Location English</label>
                    <input required  type="text" name="location_en" value=" {{  old('location_en') }}"   class="form-control" placeholder="Enter Location"/>
                </div>
                <div class="col-lg-6">
                    <label>Location Arabic</label>
                    <input required  type="text" name="location_ar" value=" {{  old('location_ar') }}"   class="form-control" placeholder="Enter Location"/>
                </div>
            </div>
            <div class="form-group row">                
                <div class="col-lg-10">
                    <label>Phone</label>
                    <input required  type="text" name="phone" value=" {{  old('phone') }}"   class="form-control" placeholder="Enter Phone"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-10">
                    <label>Store Image<span class="text-danger">*</span></label>
                    <div class="image-input image-input-outline" id="kt_image_4" >
                        <div class="image-input-wrapper" style="background-image: url()"></div>
                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar" name="change-image">
                        <i class="fa fa-pen icon-sm text-muted"></i>
                        <input type="file" name="image" accept=".png, .jpg, .jpeg"/>
                        <input type="hidden" name="profile_avatar_remove"/>
                        </label>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-10">
                    <label>Store Cover Image<span class="text-danger">*</span></label>
                    <div class="image-input image-input-outline" id="kt_image_5" >
                        <div class="image-input-wrapper" style="background-image: url()"></div>
                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar" name="change-image">
                        <i class="fa fa-pen icon-sm text-muted"></i>
                        <input type="file" name="cover" accept=".png, .jpg, .jpeg"/>
                        <input type="hidden" name="profile_avatar_remove"/>
                        </label>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Store Slogan English<span class="text-danger">*</span></label>
                    <input required  type="text" name="slogan_en" value="{{ old('slogan_en') }}"  class="form-control" placeholder="Enter Name"/>
                </div>
                <div class="col-lg-6">
                    <label>Store Slogan Arabic<span class="text-danger">*</span></label>
                    <input required  type="text" name="slogan_ar" value="{{ old('slogan_en') }}"  class="form-control" placeholder="Enter Name"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Active Status<span class="text-danger">*</span></label>
                    <select required  class="form-control" name="is_active">
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </div>
                <div class="col-lg-6">
                    <label>Hot Price Adding Allowed<span class="text-danger">*</span></label>
                    <select required  class="form-control" name="hot_price_adding_allowed">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
            </div>
                
            <div class="form-group row">
            <div class="col-lg-6">
                    <label for="">Cities</label>
                    <select name="cities[]" id="store-add-cities-multiselect" class="form-control mt-multiselect"  multiple="multiple">
                    </select>
                </div>
                <div class="col-lg-6">
                    <label for="">Areas</label>
                    <select name="areas[]" id="store-add-areas-multiselect" class="form-control mt-multiselect"  multiple="multiple">
                    </select>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-6"></div>
                <div class="col-lg-8">                    
                    <button href="#q" id="category-submit" type="submit"  class="btn btn-primary mr-2">Save</button>
                    <a href="#" id="category-cancel"  class="btn btn-secondary mr-2">cancel</a>
                </div>
            </div>
        </div>
</form>
</div>

@endsection

@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/plugins/bootstrap-multiselect.css" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript" src="/js/plugins/bootstrap-multiselect.js"></script>
<script>
    var avatar4 = new KTImageInput('kt_image_4');
    var avatar5 = new KTImageInput('kt_image_5');       
    $('#store-add-cities-multiselect').multiselect({
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true
    });
    $('#store-add-areas-multiselect').multiselect({
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true
    });
    $.ajax({
                type: "GET",
                url: '/api/cities?language=en',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer 2|eCpELKhk7BPlE1avdZwQzbeB9djAxaicpZa5k5Hp');
                },
                success: function(data){
                    $.each(JSON.parse(data), function(key,val) {
                        $('#store-add-cities-multiselect').append($("<option></option>")
                                            .attr("value", val.id)
                                            .text(val.name)); 
                    });
                    $("#store-add-cities-multiselect").multiselect('rebuild');           
                }
    });
    $.ajax({
                type: "GET",
                url: '/api/areas?language=en',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer 2|eCpELKhk7BPlE1avdZwQzbeB9djAxaicpZa5k5Hp');
                },
                success: function(data){
                    $.each(JSON.parse(data), function(key,val) {
                        $('#store-add-areas-multiselect').append($("<option></option>")
                                            .attr("value", val.id)
                                            .text(val.name)); 
                    });
                    $("#store-add-areas-multiselect").multiselect('rebuild');           
                }
    });
</script>
@endsection
