{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom example example-compact">
    <div class="card-header">
        <h3 class="card-title">
            View {{Str::plural($className)}} #{{ $entity->id }}
        </h3>
        <div class="card-toolbar">
            <div class="example-tools justify-content-center">
                <a href="{{ route('stores.index') }}" class="btn btn-secondary">Go Back</a>
            </div>
        </div>
    </div>
    <form action="{{ route('stores.update',$entity->id) }}" id="store-update-form" method="POST" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        {{csrf_field()}}
         <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Title English<span class="text-danger">*</span></label>
                    <input type="text" name="title_en" value="{{ $entity->translations->where('language_id',get_language_id_by_iso('en'))->first()->title??null }}" disabled class="form-control" placeholder="Enter Title"/>
                </div>
                <div class="col-lg-6">
                    <label>Title Arabic<span class="text-danger">*</span></label>
                    <input type="text" name="title_ar" value="{{ $entity->translations->where('language_id',get_language_id_by_iso('ar'))->first()->title??null }}" disabled class="form-control" placeholder="Enter Title"/>
                </div>
            </div>
            <div class="form-group row">                
                <div class="col-lg-6">
                    <label>Location English</label>
                    <input type="text" name="location_en" value=" {{  $entity->translations->where('language_id',get_language_id_by_iso('en'))->first()->location_text??null }}" disabled  class="form-control" placeholder="Enter Location"/>
                </div>
                <div class="col-lg-6">
                    <label>Location Arabic</label>
                    <input type="text" name="location_ar" value=" {{  $entity->translations->where('language_id',get_language_id_by_iso('ar'))->first()->location_text??null }}" disabled  class="form-control" placeholder="Enter Location"/>
                </div>
            </div>
            <div class="form-group row">                
                <div class="col-lg-10">
                    <label>Phone</label>
                    <input type="text" name="phone" value=" {{  $entity->phone??null }}" disabled  class="form-control" placeholder="Enter Phone"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-10">
                    <label>Store Image<span class="text-danger">*</span></label>
                    <div class="image-input image-input-outline" id="kt_image_4" >
                        <div class="image-input-wrapper" style="background-image: url({{$entity->image_url}})"></div>
                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar" name="change-image">
                        <i class="fa fa-pen icon-sm text-muted"></i>
                        <input type="file" name="image" accept=".png, .jpg, .jpeg"/>
                        <input type="hidden" name="profile_avatar_remove"/>
                        </label>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-10">
                    <label>Store Cover Image<span class="text-danger">*</span></label>
                    <div class="image-input image-input-outline" id="kt_image_5" >
                        <div class="image-input-wrapper" style="background-image: url({{$entity->cover_image_url}})"></div>
                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar" name="change-image">
                        <i class="fa fa-pen icon-sm text-muted"></i>
                        <input type="file" name="cover" accept=".png, .jpg, .jpeg"/>
                        <input type="hidden" name="profile_avatar_remove"/>
                        </label>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Store Slogan English<span class="text-danger">*</span></label>
                    <input type="text" name="slogan_en" value="{{ $entity->translations->where('language_id',get_language_id_by_iso('en'))->first()->slogan??null }}" disabled class="form-control" placeholder="Enter Name"/>
                </div>
                <div class="col-lg-6">
                    <label>Store Slogan Arabic<span class="text-danger">*</span></label>
                    <input type="text" name="slogan_ar" value="{{ $entity->translations->where('language_id',get_language_id_by_iso('ar'))->first()->slogan??null }}" disabled class="form-control" placeholder="Enter Name"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Active Status<span class="text-danger">*</span></label>
                    <select class="form-control" name="is_active">
                        <option value="1">Active</option>
                        <option value="0">Inactive</option>
                    </select>
                </div>
                <div class="col-lg-6">
                    <label>Hot Price Adding Allowed<span class="text-danger">*</span></label>
                    <select class="form-control" name="hot_price_adding_allowed">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-6"></div>
                <div class="col-lg-8">                    
                    <button href="#q" id="category-submit" type="submit"  class="btn btn-primary mr-2">Save</button>
                    <a href="#" id="category-edit-enable"  class="btn btn-primary mr-2">Edit</a>
                    <a href="#" id="category-cancel"  class="btn btn-secondary mr-2">cancel</a>
                </div>
            </div>
        </div>
</form>
</div>

@endsection

@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    var avatar4 = new KTImageInput('kt_image_4');
    var avatar5 = new KTImageInput('kt_image_5');

    $('#change-image').hide();
    $('#category-submit').hide();

    $('#category-edit-enable').click(function(e){
        e.preventDefault();
        $('#store-update-form :input').each(function(){
            $(this).removeAttr("disabled");
        });
        // $('[name=title_en],[name=title_ar]').removeAttr("disabled");
        $('#change-image').show();
        $('#category-submit').fadeIn();
    });    

        

</script>
@endsection
