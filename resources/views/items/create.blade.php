{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom example example-compact">
    <div class="card-header">
        <h3 class="card-title">
            Create new Item
        </h3>
        <div class="card-toolbar">
            <div class="example-tools justify-content-center">
                <a href="{{ route('categories.index') }}" class="btn btn-secondary">Go Back</a>
            </div>
        </div>
    </div>
    @if ($errors->any()){{$errors}}
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <form action="{{ route('items.store') }}" method="POST" enctype="multipart/form-data">
        
        {{csrf_field()}}
         <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Title English<span class="text-danger">*</span></label>
                    <input type="text" name="title_en" value="{{old('title')}}"  class="form-control" placeholder="Enter item name"/>
                </div>
                <div class="col-lg-6">
                    <label>Title Arabic<span class="text-danger">*</span></label>
                    <input type="text" name="title_ar" value="{{old('title')}}"  class="form-control" placeholder="أدخل عنوان البند"/>
                </div>
            </div>
            <div class="form-group row">                
                <div class="col-lg-6">
                    <label>Description English</label>
                    <textarea name="description_en" id="" cols="30" rows="10" class="form-control">                        
                    </textarea>
                </div>
                <div class="col-lg-6">
                    <label>Description Arabic</label>
                    <textarea name="description_ar" id="" cols="30" rows="10" class="form-control">                        
                    </textarea>
                </div>
            </div>
            <div class="form-group row">
                    <div class="col-lg-6">
                        <label>Category</label>
                        <select class="form-select form-control" name="category_id" data-control="select2" id="category-select" data-placeholder="Select an option">
                            <option>No categories</option>
                        </select>
                        <span class="form-text text-muted">Category of the item</span>
                    </div>
                    <div class="col-lg-6">
                        <label>Sub Category</label>
                        <select class="form-select form-control" name="sub_category_id" data-control="select2"  id="sub-category-select" data-placeholder="Select an option">
                            <option>No categories</option>
                        </select>
                        <span class="form-text text-muted">Sub category of the item</span>
                    </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Title Author<span class="text-danger">*</span></label>
                    <input type="text" name=""  value="to do"  class="form-control" placeholder="Enter Name" disabled />
                    <input type="hidden" name="title_author_id"  value="1"  class="form-control" placeholder="Enter Name" />
                </div>
                <div class="col-lg-6">
                    <label>Description Author<span class="text-danger">*</span></label>
                    <input type="text" name="" value="to do"  class="form-control" placeholder="Enter Name" disabled/>
                    <input type="hidden" name="des_author_id"  value="1"  class="form-control" placeholder="Enter Name" />
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Price<span class="text-danger">*</span></label>
                    <input type="text" name="price" value=""  class="form-control" placeholder="Enter Name"/>
                </div>
                <div class="col-lg-6">
                    <label>New Price<span class="text-danger">*</span></label>
                    <input type="text" name="new_price" value=""  class="form-control" placeholder="Enter Name"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Arrival Range<span class="text-danger">*</span></label>
                    <input type="text" name="arrival_range" value=""  class="form-control" placeholder="Enter Name"/>
                </div>
                <div class="col-lg-6">
                    <label>Stock<span class="text-danger">*</span></label>
                    <input type="number" name="in_stock" value=""  class="form-control" placeholder="Enter Name"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-12">
                    <label>Stores<span class="text-danger">*</span></label><br>
                    <select name="stores[]" id="stores_select" class="form-control mt-multiselect"  multiple="multiple">

                    </select>
                </div>
            </div>                
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Image<span class="text-danger">*</span></label>
                    <div class="user-image mb-3 text-center">
                <div class="imgPreview"> </div>
            </div>            
            <div class="custom-file">
                <input type="file" name="imageFile[]" class="custom-file-input" id="images" multiple="multiple">
                <label class="custom-file-label" for="images">Choose image</label>
            </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-6"></div>
                <div class="col-lg-8">
                    <button href="#q"  type="submit"  class="btn btn-primary mr-2">Save</button>
                    <a href="#" id="category-cancel"  class="btn btn-primary mr-2">cancel</a>
                </div>
            </div>
        </div>
</form>
</div>

@endsection

@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<link rel="stylesheet" href="/css/plugins/bootstrap-multiselect.css" type="text/css"/>
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script type="text/javascript" src="/js/plugins/bootstrap-multiselect.js"></script>
<script>
    var avatar4 = new KTImageInput('kt_image_4');

        var multiImgPreview = function(input, imgPreviewPlaceholder) {
            if (input.files) {
                var filesAmount = input.files.length;
                for (i = 0; i < filesAmount; i++) {
                    var reader = new FileReader();
                    reader.onload = function(event) {
                        $($.parseHTML('<img>'))
                            .attr('src', event.target.result)
                            .attr('width', '100px')
                            .attr('class', 'image-title')
                            .appendTo(imgPreviewPlaceholder);
                    }
                    reader.readAsDataURL(input.files[i]);
                }
            }
        };
        $('#images').on('change', function() {
            multiImgPreview(this, 'div.imgPreview');
        });

        $.ajax({
            type: "GET",
            url: '/api/categories?language=ar',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer 2|eCpELKhk7BPlE1avdZwQzbeB9djAxaicpZa5k5Hp');
            },
            success: function(data){
                $('#category-select').empty()
                $.each(JSON.parse(data), function(key,val) {
                    $('#category-select').append($("<option></option>")
                                            .attr("value", val.id)
                                            .text(val.title)); 
                });
                
               
                
            }
        });

        $.ajax({
            type: "GET",
            url: '/api/sub-categories?language=ar',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer 2|eCpELKhk7BPlE1avdZwQzbeB9djAxaicpZa5k5Hp');
            },
            success: function(data){
                $('#sub-category-select').empty()
                $.each(JSON.parse(data), function(key,val) {
                    $('#sub-category-select').append($("<option></option>")
                                            .attr("value", val.id)
                                            .text(val.title)); 
                 } );
                
               
                
            }
        });

        $('#stores_select').multiselect({
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true
        });

        $.ajax({
            type: "GET",
            url: '/api/stores?language=en',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer 2|eCpELKhk7BPlE1avdZwQzbeB9djAxaicpZa5k5Hp');
            },
            success: function(data){
                $('#stores_select').empty()
                $.each(JSON.parse(data), function(key,val) {
                    $('#stores_select').append($("<option></option>")
                                            .attr("value", val.id)
                                            .text(val.title)); 
                 });
                $("#stores_select").multiselect('rebuild');                   
            }
        });

        $('#category-select').on('change', function() {
            $.ajax({
                type: "GET",
                url: '/api/categories/'+this.value+'/sub-categories?language=ar',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer 2|eCpELKhk7BPlE1avdZwQzbeB9djAxaicpZa5k5Hp');
                },
                success: function(data){
                    $('#sub-category-select').empty()
                    $.each(JSON.parse(data), function(key,val) {
                        $('#sub-category-select').append($("<option></option>")
                                                .attr("value", val.id)
                                                .text(val.title)); 
                    });
                    
                
                    
                }
            });
        });

</script>
@endsection
