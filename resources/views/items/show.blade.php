{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom example example-compact">
    <div class="card-header">
        <h3 class="card-title">
            View {{Str::plural($className)}}
        </h3>
        <div class="card-toolbar">
            <div class="example-tools justify-content-center">
                <a href="{{ route('categories.index') }}" class="btn btn-secondary">Go Back</a>
            </div>
        </div>
    </div>
    <form action="{{ route('items.update',$entity->id) }}" method="POST" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        {{csrf_field()}}
         <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>ID<span class="text-danger">*</span></label>
                    <input type="text" name="id" value="{{ $entity->id }}" required disabled class="form-control"/>
                    <span class="form-text text-muted">Cant edit ID</span>
                </div>
                <div class="col-lg-6">
                    <label>Title<span class="text-danger">*</span></label>
                    <input type="text" name="title_en" value="{{ $entity->translations->where('language_id',session('language'))->first()->title??null }}" disabled class="form-control" placeholder="Enter Name"/>
                </div>
            </div>
            <div class="form-group row">                
                <div class="col-lg-10">
                    <label>Description</label>
                    <!-- <input type="text" name="title_ar" value="{{  $entity->translations->where('language_id',session('language'))->first()->description??null }}" disabled  class="form-control" placeholder="Enter Local Name"/> -->
                    <textarea name="" id="" cols="30" rows="10" class="form-control" disabled>
                        {{  $entity->translations->where('language_id',session('language'))->first()->description??null }}
                    </textarea>
                </div>
            </div>
            <div class="form-group row">
                    <div class="col-lg-6">
                        <label>Category</label>
                        <!-- <input type="text" name="id" value="{{ $entity->id }}" required disabled class="form-control"/> -->
                        <select class="form-select form-control" data-control="select2" id="category-select" data-placeholder="Select an option">
                            <option></option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                        </select>
                        <span class="form-text text-muted">Category of the item</span>
                    </div>
                    <div class="col-lg-6">
                        <label>Sub Category</label>
                        <!-- <input type="text" name="title_en" value="{{ $entity->translations->where('language_id',session('language'))->first()->title??null }}" disabled class="form-control" placeholder="Enter Name"/> -->
                        <select class="form-select form-control" data-control="select2"  id="sub-category-select" data-placeholder="Select an option">
                            <option></option>
                            <option value="1">Option 1</option>
                            <option value="2">Option 2</option>
                        </select>
                        <span class="form-text text-muted">Sub category of the item</span>
                    </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Title Author<span class="text-danger">*</span></label>
                    <input type="text" name="author" value="{{ $entity->titleAuthor->name }}" disabled class="form-control" placeholder="Enter Name"/>
                </div>
                <div class="col-lg-6">
                    <label>Description Author<span class="text-danger">*</span></label>
                    <input type="text" name="author" value="{{ $entity->descriptionAuthor->name }}" disabled class="form-control" placeholder="Enter Name"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Price<span class="text-danger">*</span></label>
                    <input type="text" name="author" value="{{ $entity->price }}" disabled class="form-control" placeholder="Enter Name"/>
                </div>
                <div class="col-lg-6">
                    <label>New Price<span class="text-danger">*</span></label>
                    <input type="text" name="author" value="{{ $entity->new_price }}" disabled class="form-control" placeholder="Enter Name"/>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Arrival Range<span class="text-danger">*</span></label>
                    <input type="text" name="author" value="{{ $entity->arrival_range }}" disabled class="form-control" placeholder="Enter Name"/>
                </div>
                <div class="col-lg-6">
                    <label>Stock<span class="text-danger">*</span></label>
                    <input type="text" name="author" value="{{ $entity->in_stock }}" disabled class="form-control" placeholder="Enter Name"/>
                </div>
            </div>
            <div class="form-group row">
                
                @if($entity->images)
                    @foreach($entity->images as $image)
                    <div class="col-lg-12">
                        <img src="{{$image->base_path}}" alt="" width="200px">
                    </div>
                    @endforeach
                @endif
            </div>
            <div class="form-group row">
                <div class="col-lg-6">
                    <label>Image<span class="text-danger">*</span></label>
                    <div class="image-input image-input-outline" id="kt_image_4" >
                        <div class="image-input-wrapper" style="background-image: url({{$entity->image_url}})"></div>
                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar" name="change-image">
                        <i class="fa fa-pen icon-sm text-muted"></i>
                        <input type="file" name="image" accept=".png, .jpg, .jpeg"/>
                        <input type="hidden" name="profile_avatar_remove"/>
                        </label>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-6"></div>
                <div class="col-lg-8">
                    <a href="#" id="category-edit-enable"  class="btn btn-primary mr-2">Edit</a>
                    <button href="#q" id="category-submit" type="submit"  class="btn btn-primary mr-2">Save</button>
                    <a href="#" id="category-cancel"  class="btn btn-primary mr-2">cancel</a>
                </div>
            </div>
        </div>
</form>
</div>

@endsection

@section('styles')
<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
@endsection

{{-- Scripts Section --}}
@section('scripts')
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script>
    var avatar4 = new KTImageInput('kt_image_4');

    $('#change-image').hide();
    $('#category-submit').hide();

    $('#category-edit-enable').click(function(e){
        e.preventDefault();
        $('[name=title_en],[name=title_ar]').removeAttr("disabled");
        $('#change-image').show();
        $('#category-submit').fadeIn();
    });

    $.ajax({
            type: "GET",
            url: '/api/categories?language=ar',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer 2|eCpELKhk7BPlE1avdZwQzbeB9djAxaicpZa5k5Hp');
            },
            success: function(data){
                $('#category-select').empty()
                $.each(JSON.parse(data), function(key,val) {
                    if(val.id=={{$entity->category_id}}){
                        $('#category-select').append($("<option selected></option>")
                                            .attr("value", val.id)
                                            .text(val.title)); 
                        return
                    }
                    $('#category-select').append($("<option></option>")
                                            .attr("value", key)
                                            .text(val.title)); 
                });
                
               
                
            }
        });

        $.ajax({
            type: "GET",
            url: '/api/sub-categories?language=ar',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', 'Bearer 2|eCpELKhk7BPlE1avdZwQzbeB9djAxaicpZa5k5Hp');
            },
            success: function(data){
                $('#sub-category-select').empty()
                $.each(JSON.parse(data), function(key,val) {
                    if(val.id=={{$entity->sub_category_id}}){
                        $('#sub-category-select').append($("<option selected></option>")
                                            .attr("value", key)
                                            .text(val.title)); 
                        return
                    }
                    $('#sub-category-select').append($("<option></option>")
                                            .attr("value", key)
                                            .text(val.title)); 
                });
                
               
                
            }
        });

        $('#category-select').on('change', function() {
            $.ajax({
                type: "GET",
                url: '/api/categories/'+this.value+'/sub-categories?language=ar',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer 2|eCpELKhk7BPlE1avdZwQzbeB9djAxaicpZa5k5Hp');
                },
                success: function(data){
                    $('#sub-category-select').empty()
                    $.each(JSON.parse(data), function(key,val) {
                        $('#sub-category-select').append($("<option></option>")
                                                .attr("value", key)
                                                .text(val.title)); 
                    });
                    
                
                    
                }
            });
        });

</script>
@endsection
