{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

    <div class="card card-custom">
        <div class="card-header flex-wrap border-0 pt-6 pb-0">
            <div class="card-title">
                <h3 class="card-label">Items
                    <div class="text-muted pt-2 font-size-sm">All Items Datatable</div>
                </h3>
            </div>
            <div class="card-toolbar">
                <!--begin::Button-->
                <a href="{{route('items.create')}}" class="btn btn-primary font-weight-bolder">
                <i class="la la-plus"></i>New Record</a>
                <!--end::Button-->
            </div>
        </div>

        <div class="card-body">
            @if ($message = Session::get('success'))
            <div id="alert" class="alert alert-custom alert-notice alert-light-success fade show" role="alert">
                <!-- <div class="alert-icon"><i class="flaticon-warning"></i></div> -->
                <div class="alert-text">{{ $message }}</div>
                <div class="alert-close">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true"><i class="ki ki-close"></i></span>
                    </button>
                </div>
            </div>
            @endif
            <table class="table table-bordered table-hover main_datatable" id="kt_datatable">
                <thead>
                <tr>
                    <th>ID</th>
                    <!-- <th>image</th> -->
                    <th>Title</th>
                    <th>Sub Category</th>
                    <th>Price</th>
                    <th>New Price</th>
                    <th>Stock</th>
                    <th>Cities</th>
                    <th>Areas</th>
                    <th>Actions</th>
                </tr>
                <tr>
                    <td>sort</td>
                    <td>sort</td>
                    <td>
                        <select name="" id="sub_category_filter" class="form-control">
                            <option value="0">Select a Sub Category</option>
                        </select>
                    </td>
                    <td>
                        <input type="number" id="price_from" class="form-control filter-input" placeholder="Price from">
                        <input type="number" id="price_to" class="form-control filter-input" placeholder="Price to">
                    </td>
                    <td>sort</td>
                    <td>
                        <input type="number" id="stock" class="form-control filter-input" placeholder="stock">
                    </td>
                    <td>
                        <select name="" id="store-add-cities-multiselect" class="form-control mt-multiselect">
                            <option value="0">Select a city</option>
                        </select>
                    </td>
                    <td>
                        <select name="" id="store-add-areas-multiselect" class="form-control mt-multiselect">
                            <option value="0">Select a area</option>
                        </select>
                    </td>
                    <td>
                        <button id="filter-btn" class="btn btn-primary">Filter</button>
                    </td>
                </tr>
                </thead>
                <tbody>
                </tbody>

            </table>

        </div>

    </div>

@endsection

{{-- Styles Section --}}
@section('styles')
    <link href="{{ asset('plugins/custom/datatables/datatables.bundle.css') }}" rel="stylesheet" type="text/css"/>  
    <link rel="stylesheet" href="/css/plugins/bootstrap-multiselect.css" type="text/css"/>
@endsection


{{-- Scripts Section --}}
@section('scripts')
    {{-- vendors --}}
    <script src="{{ asset('plugins/custom/datatables/datatables.bundle.js') }}" type="text/javascript"></script>
    <script type="text/javascript" src="/js/plugins/bootstrap-multiselect.js"></script> 

    {{-- page scripts --}}
    <script type="text/javascript">
        $(function () {
            
            var table = $('.main_datatable').DataTable({
                processing: true,
                serverSide: true,
                destroy: true,
                ajax: "#",
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'title', name: 'title'},
                    {data: 'sub_category', name: 'sub_category'},
                    {data: 'price', name: 'price'},
                    {data: 'new_price', name: 'new_price'},
                    {data: 'in_stock', name: 'in_stock'},                    
                    {data: 'cities', name: 'cities'},
                    {data: 'areas', name: 'areas'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });

            $('#filter-btn').click(function(){
                var sub_category=$('#sub_category_filter').find(":selected").val();
                var city=$('#store-add-cities-multiselect').find(":selected").val();
                var area=$('#store-add-areas-multiselect').find(":selected").val();
                var stock=$('#stock').val();
                var price_from=$('#price_from').val()
                var price_to=$('#price_to').val()
                
                var table = $('.main_datatable').DataTable({
                processing: true,
                serverSide: true,
                destroy: true,
                ajax: "/items?price_from="+price_from+"&price_to="+price_to+"&sub_category="+sub_category+"&city="+city+"&area="+area+"&stock="+stock,
                columns: [
                    {data: 'id', name: 'id'},
                    {data: 'title', name: 'title'},
                    {data: 'sub_category', name: 'sub_category'},
                    {data: 'price', name: 'price'},
                    {data: 'new_price', name: 'new_price'},
                    {data: 'in_stock', name: 'in_stock'},                    
                    {data: 'cities', name: 'cities'},
                    {data: 'areas', name: 'areas'},
                    {data: 'action', name: 'action', orderable: false, searchable: false},
                ]
            });
            });
            $.ajax({
                type: "GET",
                url: '/api/sub-categories?language=en',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer 2|eCpELKhk7BPlE1avdZwQzbeB9djAxaicpZa5k5Hp');
                },
                success: function(data){
                    $('#sub-category-select').empty()
                    $.each(JSON.parse(data), function(key,val) {
                        $('#sub_category_filter').append($("<option></option>")
                                                .attr("value", val.id)
                                                .text(val.title)); 
                    });
                    
                
                    
                }
            });
        });

        $('#store-add-cities-multiselect').multiselect({
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true
    });
    $('#store-add-areas-multiselect').multiselect({
            enableFiltering: true,
            enableCaseInsensitiveFiltering: true
    });
    $.ajax({
                type: "GET",
                url: '/api/cities?language=en',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer 2|eCpELKhk7BPlE1avdZwQzbeB9djAxaicpZa5k5Hp');
                },
                success: function(data){
                    $.each(JSON.parse(data), function(key,val) {
                        $('#store-add-cities-multiselect').append($("<option></option>")
                                            .attr("value", val.id)
                                            .text(val.name)); 
                    });
                    $("#store-add-cities-multiselect").multiselect('rebuild');           
                }
    });
    $.ajax({
                type: "GET",
                url: '/api/areas?language=en',
                beforeSend: function (xhr) {
                    xhr.setRequestHeader('Authorization', 'Bearer 2|eCpELKhk7BPlE1avdZwQzbeB9djAxaicpZa5k5Hp');
                },
                success: function(data){
                    $.each(JSON.parse(data), function(key,val) {
                        $('#store-add-areas-multiselect').append($("<option></option>")
                                            .attr("value", val.id)
                                            .text(val.name)); 
                    });
                    $("#store-add-areas-multiselect").multiselect('rebuild');           
                }
    });

        $('body').on('click', '.delete', function () {
            var id = $(this).data('id');
            Swal.fire({
                title: "Are you sure?",
                text: "You won't be able to revert this!",
                icon: "warning",
                showCancelButton: true,
                confirmButtonText: "Yes, delete it!"
            }).then(function(result) {
                if (result.value) {

                    // ajax
                    $.ajax({
                        type:"DELETE",
                        url: "/categories/"+id,
                        data:{
                            '_token': '{{ csrf_token() }}',
                        },
                        dataType: 'json',
                        success: function(res){
                            $('.main_datatable').DataTable().ajax.reload();
                        }
                    });
                    Swal.fire(
                        "Deleted!",
                        "Record has been deleted.",
                        "success"
                    );
                }
            });
        });
    </script>
    @if ($message = Session::get('success'))
    <script>
        $('#alert').show();
            setTimeout(function() {
                $('#alert').hide();
        }, 5000);
    </script>
    @endif
@endsection
