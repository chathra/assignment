{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom example example-compact">
    <div class="card-header">
        <h3 class="card-title">
            View {{Str::plural($className)}} # &nbsp {{ $entity->id }}
        </h3>
        <div class="card-toolbar">
            <div class="example-tools justify-content-center">
            <a href="{{ route('categories.sub-categories.index',['categoryId'=>$entity->category_id]) }}" class="btn btn-secondary">Go Back</a>
            </div>
        </div>
    </div>
    <form action="{{ route('categories.sub-categories.update',['categoryId'=>$entity->category_id,'subCategoryId'=>$entity->id]) }}" method="POST" enctype="multipart/form-data">
        {{ method_field('PUT') }}
        {{csrf_field()}}
         <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-4">
                    <label>ID<span class="text-danger">*</span></label>
                    <input type="text" name="id" value="{{ $entity->id }}" required disabled class="form-control"/>
                    <span class="form-text text-muted">Cant edit ID</span>
                </div>
                <div class="col-lg-4">
                    <label>Title English<span class="text-danger">*</span></label>
                    <input type="text" name="title_en" value="{{ $entity->translations()->where('language_id',get_language_id_by_iso('en'))->first()->title??null }}" disabled class="form-control" placeholder="Enter Name"/>
                    <span class="form-text text-muted">Please enter English Title, Max 50 character allowed</span>
                </div>
                <div class="col-lg-4">
                    <label>Title Arabic</label>
                    <input type="text" name="title_ar" value="{{ $entity->translations()->where('language_id',get_language_id_by_iso('ar'))->first()->title??null }}" disabled  class="form-control" placeholder="Enter Local Name"/>
                    <span class="form-text text-muted">Please enter Arabic Title, Max 50 character allowed</span>
                </div>
            <!-- </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    <label>Image<span class="text-danger">*</span></label>
                    <img src="{{$entity->image_url}}" height="100px" >
                </div>
                
            </div> -->
            <div class="form-group row">
                <div class="col-lg-4">
                    <label>Image<span class="text-danger">*</span></label>
                    <div class="image-input image-input-outline" id="kt_image_4" >
                        <div class="image-input-wrapper" style="background-image: url({{$entity->image_url}})"></div>
                        <label class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="change" data-toggle="tooltip" title="" data-original-title="Change avatar" name="change-image">
                        <i class="fa fa-pen icon-sm text-muted"></i>
                        <input type="file" name="image" accept=".png, .jpg, .jpeg"/>
                        <input type="hidden" name="profile_avatar_remove"/>
                        </label>

                        <span class="btn btn-xs btn-icon btn-circle btn-white btn-hover-text-primary btn-shadow" data-action="cancel" data-toggle="tooltip" title="Cancel avatar">
                        <i class="ki ki-bold-close icon-xs text-muted"></i>
                        </span>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-8">
                    <a href="#" id="category-edit-enable"  class="btn btn-primary mr-2">Edit</a>
                    <button href="#q" id="category-submit" type="submit"  class="btn btn-primary mr-2">Save</button>
                    <a href="#" id="category-cancel"  class="btn btn-primary mr-2">cancel</a>
                </div>
            </div>
        </div>
</form>
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    var avatar4 = new KTImageInput('kt_image_4');

    $('#change-image').hide();
    $('#category-submit').hide();

    $('#category-edit-enable').click(function(e){
        e.preventDefault();
        $('[name=title_en],[name=title_ar]').removeAttr("disabled");
        $('#change-image').show();
        $('#category-submit').fadeIn();
    });


</script>
@endsection
