{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom example example-compact">
    <div class="card-header">
        <h3 class="card-title">
            Add New Sub Category
        </h3>
        <div class="card-toolbar">
            <div class="example-tools justify-content-center">
            <a href="{{ route('categories.index') }}" class="btn btn-secondary">Go Back</a>
            </div>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <strong>Whoops!</strong> There were some problems with your input.<br><br>
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
    <!--begin::Form-->
    <form action="{{ route('categories.sub-categories.store',$categoryId) }}" method="POST" enctype="multipart/form-data" id="category_create_form">
        @csrf
        <input type="hidden" name="category_id" value="{{$categoryId}}">
        <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-4">
                    <label>Title English<span class="text-danger">*</span></label>
                    <input type="text" name="title_en"  class="form-control" placeholder="Enter English Name"/>
                    <span class="form-text text-muted">Please enter English Name, Max 50 character allowed</span>
                </div>
                <div class="col-lg-4">
                    <label>Title Arabic</label>
                    <input type="text" name="title_ar" class="form-control" placeholder="Enter Arabic Name"/>
                    <span class="form-text text-muted">Please enter Arabic Name, Max 50 character allowed</span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    <label>Image<span class="text-danger">*</span></label>
                    <div class="image-input image-input-empty image-input-outline" id="kt_image_5" style="background-image: url(/media/users/blank.png)">
                        <input type="file" name="image" accept=".png, .jpg, .jpeg"/>
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-8">
                    <button type="submit" class="btn btn-primary mr-2">Submit</button>
                    <button type="reset" class="btn btn-secondary">Cancel</button>
                </div>
            </div>
        </div>
    </form>
    <!--end::Form-->
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    @if(session()->has('success'))
    swal.fire({
            title: "{{ session()->get('success') }}",
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
   @endif

    var avatar5 = new KTImageInput('kt_image_5');

    avatar5.on('cancel', function(imageInput) {
        swal.fire({
            title: 'Image successfully changed !',
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    avatar5.on('change', function(imageInput) {
        swal.fire({
            title: 'Image successfully changed !',
            type: 'success',
            buttonsStyling: false,
            confirmButtonText: 'Awesome!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

    avatar5.on('remove', function(imageInput) {
        swal.fire({
            title: 'Image successfully removed !',
            type: 'error',
            buttonsStyling: false,
            confirmButtonText: 'Got it!',
            confirmButtonClass: 'btn btn-primary font-weight-bold'
        });
    });

</script>
@endsection
