{{-- Extends layout --}}
@extends('layout.default')

{{-- Content --}}
@section('content')

<div class="card card-custom example example-compact">
    <div class="card-header">
        <h3 class="card-title">
            View Country
        </h3>
        <div class="card-toolbar">
            <div class="example-tools justify-content-center">
            <a href="{{ route('countries.index') }}" class="btn btn-secondary">Go Back</a>
            </div>
        </div>
    </div>
    <!--begin::Form-->
         <div class="card-body">
            <div class="form-group row">
                <div class="col-lg-4">
                    <label>ID<span class="text-danger">*</span></label>
                    <input type="text" name="id" value="{{ $country->id }}" required disabled class="form-control"/>
                    <span class="form-text text-muted">Cant edit ID</span>
                </div>
                <div class="col-lg-4">
                    <label>Name English<span class="text-danger">*</span></label>
                    <input type="text" name="name_en" value="{{ $country->name_en }}" required disabled class="form-control" placeholder="Enter Name"/>
                    <span class="form-text text-muted">Please enter Name, Max 50 character allowed</span>
                </div>
                <div class="col-lg-4">
                    <label>Name Arabic</label>
                    <input type="text" name="name_ar" value="{{ $country->name_ar }}" disabled  class="form-control" placeholder="Enter Local Name"/>
                    <span class="form-text text-muted">Please enter Local Name, Max 50 character allowed</span>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-lg-4">
                    <label>Flag<span class="text-danger">*</span></label>
                    <img src="{{$country->flag_url}}" height="100px" >
                    <span class="form-text text-muted">Please enter Phone, Max 6 character allowed</span>
                </div>
            </div>
        </div>
        <div class="card-footer">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-8">
                    <a href="{{ route('countries.edit',$country->id) }}"  class="btn btn-primary mr-2">Edit</a>
                </div>
            </div>
        </div>

    <!--end::Form-->
</div>

@endsection

{{-- Scripts Section --}}
@section('scripts')
<script>
    var avatar4 = new KTImageInput('kt_image_4');


</script>
@endsection
