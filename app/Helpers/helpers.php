<?php

if(!function_exists('get_language_id_by_iso')){
    function get_language_id_by_iso(string $iso)
    {
        return \App\Models\Language::where('iso',$iso)->first()->id;
    }
}
    
