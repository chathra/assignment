<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;


abstract class BaseRepository
{
	protected $model;

	public function __construct(Model $model)
	{
		$this->model=$model;
	}

	public function fetchAll($paginationRequired=false)
	{
		return $paginationRequired ?
					$this->model::paginate(10):
					$this->model::all();
	}

	public function getFilter()
	{
		return $this->model->newQuery();
	}

	public function applyFilter($filter)
	{
		return $filter->get();
	}

	public function fetchOne($field,$value)
	{
		return $this->model::where($field,$value)->first();
	}

	public function save(array $data)
	{
		return $this->model::create($data);
	}

	protected function saveImage($image,$path)
	{
        $imageName = time().'.'.$image->extension();  
     
        $image->move(storage_path('app/public/'.$path), $imageName);

		return "/storage/$path/$imageName";

	}

	public function update($id,$data)
	{
		$model=$this->model::whereId($id)->update($data->only($this->model->getFillable()));
	}

	public function delete($id)
	{
		$this->model::whereId($id)->delete();
	}

}