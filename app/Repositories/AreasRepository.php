<?php

namespace App\Repositories;

use App\Models\Area;

class AreasRepository extends BaseRepository
{
    public function __construct(Area $area)
    {
        $this->model=$area;
    }

    public function save($data=[])
    {
        $this->model::create($data);
    }
}