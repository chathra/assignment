<?php

namespace App\Repositories;

use App\Models\Item;

class ItemsRepository extends BaseRepository
{
    protected $imageSavePath='app/public/items/images';

    public function __construct(Item $item)
    {
        $this->model=$item;
    }
    
    public function save($request)
    {
        \DB::beginTransaction();

        try{
            $item=parent::save($request->only(
                $this->model->getFillable()
            ));
            
            if($request->stores){
                $item->stores()->attach($request->stores);
            }          
            
    
            if($request->has('imageFile')){
                foreach($request->imageFile as $image){
                    $imageUrl=$this->saveImage($image,$this->imageSavePath);
    
                    \App\Models\ItemImage::create([
                        'item_id'=>$item->id,
                        'base_path'=>$imageUrl,
                        'thumbnail_path'=> $imageUrl,
                        'type'=>$image->getClientOriginalExtension()
                    ]);
                }
            }

            $data=$request->all(); 
            
            $this->saveTranslations(
                $item->id,
                get_language_id_by_iso('en'),
                $data['title_en'],
                $data['description_en']
            );

            $this->saveTranslations(
                $item->id,
                get_language_id_by_iso('ar'),
                $data['title_ar'],
                $data['description_ar']
            );   


            \DB::commit();
        }catch(\Exception $e){dd($e->getMessage());
            \DB::rollback();
            return $e->getMessage();
        }

    }


    public function update($id,$request)
    {
        \DB::beginTransaction();
        try{
            $category=$this->fetchOne('id',$id);

            if($request->hasFile('image')){
                $imageUrl=$this->saveImage($request->image,$this->imageSavePath);

                $category->update([
                    'image_url'=>$imageUrl
                ]);
            }

            $data=$request->all();
    

            if(!empty($data['title_en'])){
                $this->updateTranslations(
                    $category->id,
                    get_language_id_by_iso('en'),
                    $data['title_en']
                );
            }
            if(!empty($data['title_en'])){
                $this->updateTranslations(
                    $category->id,
                    get_language_id_by_iso('ar'),
                    $data['title_ar']
                );
            } 

            \DB::commit();
        }catch(\Exception $e){
            \DB::rollback();

            return $e->getMessage();
        }
    }

    public function saveTranslations($itemId,$languageId,$title,$description)
    {
        \App\Models\ItemTranslation::create([
            'item_id'=>$itemId,
            'language_id'=>$languageId,
            'title'=>$title,
            'description'=>$description
            ]);
    }

    public function updateTranslations($categoryId,$languageId,$title)
    {

        \App\Models\CategoryTranslation::where('category_id',$categoryId)
            ->where('language_id',$languageId)
            ->update([
                'title'=>$title
            ]);
    }

}