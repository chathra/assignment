<?php

namespace App\Repositories;

use App\Models\Country;

class CountriesRepository extends BaseRepository
{
    public function __construct(Country $country)
    {
        $this->model=$country;
    }

    public function save($data=[])
    {
        $this->model::create($data);
    }
}