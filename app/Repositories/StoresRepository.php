<?php

namespace App\Repositories;

use App\Models\Store;

class StoresRepository extends BaseRepository
{
    protected $imageSavePath='app/public/stores/images'; 

    public function __construct(Store $item)
    {
        $this->model=$item;
    }

    public function save($request)
    {
        \DB::beginTransaction();

        try{
            
            $store=parent::save(
                $request->only(
                $this->model->getFillable())
                +
                [
                    'title_author_id'=>1,
                    'store_location_text_author_id'=>1
                ]

            );

            if($request->has('areas')){
                $store->areas()->attach($request->areas);
            }

            if($request->has('cities')){
                $store->cities()->attach($request->cities);
            }            
    
            if($request->hasFile('image')){
                $imageUrl=$this->saveImage($request->image,$this->imageSavePath);

                $store->update([
                    'image_url'=>$imageUrl
                ]);
            }

            if($request->hasFile('cover')){
                $CoverimageUrl=$this->saveImage($request->cover,$this->imageSavePath);

                $store->update([
                    'cover_image_url'=>$CoverimageUrl
                ]);
            }

            $data=$request->all();   

            
            $this->saveTranslations(
                $store->id,
                get_language_id_by_iso('en'),
                $data['title_en'],
                $data['location_en'],
                $data['slogan_en'],
            );

            
            $this->saveTranslations(
                $store->id,
                get_language_id_by_iso('ar'),
                $data['title_ar'],
                $data['location_ar'],
                $data['slogan_ar'],
            );


            \DB::commit();
        }catch(\Exception $e){dd($e->getMessage());
            \DB::rollback();
            return $e->getMessage();
        }

    }


    public function update($id,$request)
    {
        $request->validate([
            'image'=>'image',
            'cover'=>'image'
        ]);

        \DB::beginTransaction();
        try{
            $store=$this->fetchOne('id',$id);

            if($request->hasFile('image')){
                $imageUrl=$this->saveImage($request->image,$this->imageSavePath);

                $store->update([
                    'image_url'=>$imageUrl
                ]);
            }

            if($request->hasFile('cover')){
                $CoverimageUrl=$this->saveImage($request->cover,$this->imageSavePath);

                $store->update([
                    'cover_image_url'=>$CoverimageUrl
                ]);
            }

            $data=$request->all();   

            
            $this->updateTranslations(
                $store->id,
                get_language_id_by_iso('en'),
                $data['title_en'],
                $data['location_en'],
                $data['slogan_en'],
            );

            
            $this->updateTranslations(
                $store->id,
                get_language_id_by_iso('ar'),
                $data['title_ar'],
                $data['location_ar'],
                $data['slogan_ar'],
            );
             

            \DB::commit();
        }catch(\Exception $e){dd($e->getMessage());
            \DB::rollback();

            return $e->getMessage();
        }
    }

    public function saveTranslations($storeId,$languageId,$title,$location,$slogan)
    {
        \App\Models\StoreTranslation::create([
            'store_id'=>$storeId,
            'language_id'=>$languageId,
            'title'=>$title,
            'location_text'=>$location,
            'slogan'=>$slogan
        ]);
    }

    public function updateTranslations($storeId,$languageId,$title,$location,$slogan)
    {

        \App\Models\StoreTranslation::where('store_id',$storeId)
            ->where('language_id',$languageId)
            ->update([
                'title'=>$title,
                'location_text'=>$location,
                'slogan'=>$slogan
            ]);
    }

}