<?php

namespace App\Repositories;

use App\Models\SubCategory;

class SubCategoriesRepository extends BaseRepository
{
    protected $imageSavePath='app/public/sub_category/images';

    public function __construct(SubCategory $subCategory)
    {
        $this->model=$subCategory;
    }

    public function fetchAllByCategory($categoryId)
    {
        return $this->model::where('category_id',$categoryId)->get();
    }

    public function save($request)
    {
        $imagePath=$this->saveImage($request->image,$this->imageSavePath);

        $category=parent::save(
            $request->only($this->model->getFillable())+
            [
                'image_url'=>$imagePath,
                'title_author_id'=>1
            ]
        );

        if($request->filled('title_en')){
            $this->saveTranslations(
                $category->id,
                get_language_id_by_iso('en'),
                $request->title_en
            );
        }

        if($request->filled('title_ar')){
            $this->saveTranslations(
                $category->id,
                get_language_id_by_iso('ar'),
                $request->title_ar
            );
        }        

        return $category;
    }

    public function update($id,$request)
    {
        \DB::beginTransaction();
        try{
            $subCategory=$this->fetchOne('id',$id);

            if($request->hasFile('image')){
                $imageUrl=$this->saveImage($request->image,$this->imageSavePath);

                $subCategory->update([
                    'image_url'=>$imageUrl
                ]);
            }

            $data=$request->all();
    

            if(!empty($data['title_en'])){
                $this->updateTranslations(
                    $subCategory->id,
                    get_language_id_by_iso('en'),
                    $data['title_en']
                );
            }
            if(!empty($data['title_en'])){
                $this->updateTranslations(
                    $subCategory->id,
                    get_language_id_by_iso('ar'),
                    $data['title_ar']
                );
            } 

            \DB::commit();
        }catch(\Exception $e){
            \DB::rollback();

            return $e->getMessage();
        }
    }

    public function saveTranslations($categoryId,$languageId,$title)
    {

        \App\Models\SubCategoryTranslation::create([
            'sub_category_id'=>$categoryId,
            'language_id'=>$languageId,
            'title'=>$title
            ]);
    }

    public function updateTranslations($categoryId,$languageId,$title)
    {

        \App\Models\SubCategoryTranslation::where('sub_category_id',$categoryId)
            ->where('language_id',$languageId)
            ->update([
                'title'=>$title
            ]);
    }

}