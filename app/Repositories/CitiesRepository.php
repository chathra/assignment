<?php

namespace App\Repositories;

use App\Models\City;

class CitiesRepository extends BaseRepository
{
    public function __construct(City $city)
    {
        $this->model=$city;
    }

    public function save($data=[])
    {
        $this->model::create($data);
    }
}