<?php

namespace App\Repositories;

use App\Models\Category;

class CategoriesRepository extends BaseRepository
{
    protected $imageSavePath='app/public/category/images';

    public function __construct(Category $category)
    {
        $this->model=$category;
    }

    public function save($data)
    {
        $imagePath=$this->saveImage($data['image'],$this->imageSavePath);

        $category=parent::save(
            $data+
            [
                'image_url'=>$imagePath
            ]
        );

        if(!empty($data['title_en'])){
            $this->saveTranslations(
                $category->id,
                get_language_id_by_iso('en'),
                $data['title_en']
            );
        }

        if(!empty($data['title_en'])){
            $this->saveTranslations(
                $category->id,
                get_language_id_by_iso('ar'),
                $data['title_ar']
            );
        }        

        return $category;
    }

    public function update($id,$request)
    {
        \DB::beginTransaction();
        try{
            $category=$this->fetchOne('id',$id);

            if($request->hasFile('image')){
                $imageUrl=$this->saveImage($request->image,$this->imageSavePath);

                $category->update([
                    'image_url'=>$imageUrl
                ]);
            }

            $data=$request->all();
    

            if(!empty($data['title_en'])){
                $this->updateTranslations(
                    $category->id,
                    get_language_id_by_iso('en'),
                    $data['title_en']
                );
            }
            if(!empty($data['title_en'])){
                $this->updateTranslations(
                    $category->id,
                    get_language_id_by_iso('ar'),
                    $data['title_ar']
                );
            } 

            \DB::commit();
        }catch(\Exception $e){
            \DB::rollback();

            return $e->getMessage();
        }
    }

    public function saveTranslations($categoryId,$languageId,$title)
    {

        \App\Models\CategoryTranslation::create([
            'category_id'=>$categoryId,
            'language_id'=>$languageId,
            'title'=>$title
            ]);
    }

    public function updateTranslations($categoryId,$languageId,$title)
    {

        \App\Models\CategoryTranslation::where('category_id',$categoryId)
            ->where('language_id',$languageId)
            ->update([
                'title'=>$title
            ]);
    }

}