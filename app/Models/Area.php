<?php

namespace App\Models;

use App\Traits\hasTranslations;
use Illuminate\Database\Eloquent\Model;
use App\Models\City;

class Area extends Model
{
    use hasTranslations;

    protected $appends=[
        'name'
    ];

    public function city()
    {
        return $this->belongsTo(City::class);
    }

    public function getNameAttribute()
    {
        if(request()->has('language')){
            $language=$this->translations->where(
            'language_id',
            get_language_id_by_iso(request('language')??1
            ))->first();

            return  $language
                    ? $language->name:'';
        }     

        
    }
}
