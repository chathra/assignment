<?php

namespace App\Models;

use App\Models\CountryTranslations;
use Illuminate\Database\Eloquent\Model;
use App\Traits\hasTranslations;

class Country extends Model
{
    use hasTranslations;

    protected $with = [
        'translations'
    ];

    protected $append=[
        'name_en',
        'name_ar',
    ];
    
    public function getNameEnAttribute()
    {
        return $this->translations->where('language_id',1)->first()->name;
    }

    public function getNameArAttribute()
    {
        return $this->translations->where('language_id',2)->first()->name;
    }
    
}
