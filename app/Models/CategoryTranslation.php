<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;


class CategoryTranslation extends Model
{
    use HasFactory;

    protected $fillable=[
        'category_id',
        'language_id',
        'title'
    ];

    protected $hidden=[
        'category_id',
        'language_id'
    ];

    protected $append=[
        'title'
    ];

}
