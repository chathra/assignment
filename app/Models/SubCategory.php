<?php

namespace App\Models;

use App\Traits\hasTranslations;
use Illuminate\Database\Eloquent\Model;

class SubCategory extends Model
{
    use hasTranslations;

    protected $with=[
        'category'
    ];

    protected $fillable=[
        'category_id',
        'title_author_id',
        'image_url'
    ];

    protected $appends=[
        'title'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function getTitleAttribute()
    {
        if(request()->has('language')){
            $language=$this->translations->where(
            'language_id',
            get_language_id_by_iso(request('language')??1
            ))->first();

            return  $language
                    ? $language->title:'';
        }     

        
    }
}
