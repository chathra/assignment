<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\hasTranslations;
use App\Models\Area;
use App\Models\City;

class Store extends Model
{
    use hasTranslations; 

    protected $fillable=[
        'title_author_id',
        'store_location_text_author_id',
        'phone',
        'latitude',
        'longitude',
        'image_url',
        'cover_image_url',
        'google_maps_link',
        'slogan_author_id ',
        'is_active',
        'hot_price_adding_allowed',
    ];

    protected $appends=[
        'title'
    ];

    public function areas()
    {
        return $this->belongsToMany(Area::class);
    }

    public function cities()
    {
        return $this->belongsToMany(City::class);
    }

    public function getTitleAttribute()
    {
        if(request()->has('language')){
            $language=$this->translations->where(
            'language_id',
            get_language_id_by_iso(request('language')??1
            ))->first();

            return  $language
                    ? $language->title:'';
        }   
    }
}
