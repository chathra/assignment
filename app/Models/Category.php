<?php

namespace App\Models;

use App\Traits\hasTranslations;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Category extends Model
{
    use hasTranslations;

    protected $fillable=[
        'title_author_id',
        'image_url'
    ];

    protected $hidden=[
        'title_author_id',
        'translations'
    ];

    protected $with=[
        'author',
        'translations'
    ];

    protected $appends=[
        'title',
    ];

    public function author()
    {
        return $this->hasOne(User::class,'id','title_author_id');
    }

    public function getTitleAttribute()
    {
        if(request()->has('language')){
            $language=$this->translations->where(
            'language_id',
            get_language_id_by_iso(request('language')??1
            ))->first();

            return  $language
                    ? $language->title:'';
        }     

        
    }

}
