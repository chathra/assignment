<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\hasTranslations;

class City extends Model
{
    use hasTranslations;

    protected $appends=[
        'name'
    ];

    protected $hidden=[
        'translations'
    ];

    public function getNameAttribute()
    {
        if(request()->has('language')){
            $language=$this->translations->where(
            'language_id',
            get_language_id_by_iso(request('language')??1
            ))->first();

            return  $language
                    ? $language->name:'';
        }     

        
    }
}
