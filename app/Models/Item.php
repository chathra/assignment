<?php

namespace App\Models;

use App\Traits\hasTranslations;
use Illuminate\Database\Eloquent\Model;
use App\Models\Category;
use App\Models\SubCategory;
use App\Models\User;
use App\Models\ItemImage;
use App\Models\Store;

class Item extends Model
{
    use hasTranslations;

    protected $fillable=[
        'category_id',
        'sub_category_id',
        'title_author_id',
        'des_author_id',
        'price',
        'new_price',
        'arrival_range',
        'in_stock'
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function subCategory()
    {
        return $this->belongsTo(SubCategory::class);
    }

    public function titleAuthor()
    {
        return $this->belongsTo(User::class,'title_author_id');
    }

    public function descriptionAuthor()
    {
        return $this->belongsTo(User::class,'des_author_id');
    }

    public function images()
    {
        return $this->hasMany(ItemImage::class);
    }

    public function stores()
    {
        return $this->belongsToMany(Store::class);
    }
}
