<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return false;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name_en'=>'required_if:name_ar',
            'name_ar'=>'required_if:name_en',
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }
}
