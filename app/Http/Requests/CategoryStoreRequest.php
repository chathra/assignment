<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CategoryStoreRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title_author_id'=>'required|exists:users,id',
            'image'=>'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
            'language_id'=>'sometimes|required|exists:languages,id',
            'title'=>'required_with:language_id'
        ];
    }
}
