<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CountriesRepository;
use App\Models\Country;
use Yajra\DataTables\Facades\DataTables;

class CountryController extends Controller
{
    protected $repo;

    public function __construct(CountriesRepository $countriesRepo)
    {
        $this->repo=$countriesRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $request=request();
        if ($request->ajax()) {
            $data = $this->repo->fetchAll();
            return DataTables::of($data)
                ->editColumn('name_en', function($data)
                {
                   return $data->translations->where('language_id',1)->first()->name;
                })
                ->editColumn('name_ar', function($data)
                {
                   return $data->translations->where('language_id',2)->first()->name;
                })
                ->addIndexColumn()
                ->addColumn('action', 'countries.action')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        $page_title = 'Countries';
        $page_description = 'This page is to show all the records in country table';

        return view('countries.index', compact('page_title', 'page_description'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Countries';
        $page_description = 'This page is to create new country';

        return view('countries.create', compact('page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name_en'=>'required|unique:country_translations.name',
            'name_ar'=>'required|unique:country_translations.name',
            'image'=>'image||mimes:png,jpeg,gif'
        ]);

        $this->repo->save($request->all);

        return back()->with(['success'=>'Record has been added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $country=$this->repo->fetchOne('id',$id);

        $page_title = 'Show Country';
        $page_description = 'This page is to show country details';
        
        return view('countries.show',compact('country', 'page_title', 'page_description'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $page_title = 'Edit Country';
        $page_description = 'This page is to edit record in country table';
        
        return view('countries.edit',compact('country', 'page_title', 'page_description'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        // $request->validate([
        //     'name_en'=>'unique:county_translations,name'
        // ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
