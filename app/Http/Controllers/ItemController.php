<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\ItemsRepository;
use Yajra\DataTables\Facades\DataTables;
use App\Traits\HasLanguages;
use Illuminate\Support\Str;

class ItemController extends Controller
{
    use HasLanguages;

    protected $repo;

    public function __construct(ItemsRepository $itemsRepo)
    {
        $this->repo=$itemsRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {       
        session(['language' => 2]);

        $filter=$this->repo->getFilter();

        if($priceFrom=request('price_from')){
            $filter->where('price','>',$priceFrom);
        }
        if($priceTo=request('price_to')){
            $filter->where('price','<',$priceTo);
        }
        if($subCategoryId=request('sub_category')){
            $filter->where('sub_category_id',$subCategoryId);
        }
        if($city=request('city')){
            $filter->whereHas('stores',function($stores)use($city){
                return $stores->whereHas('cities',function($cities)use($city){
                    return $cities->where('city_id',$city);
                });
            });
        }
        if($area=request('area')){
            $filter->whereHas('stores',function($stores)use($area){
                return $stores->whereHas('areas',function($cities)use($area){
                    return $cities->where('area_id',$area);
                });
            });
        }
        if($stock=request('stock')){
            $filter->where('in_stock','>=',$stock);
        }

        
        $data = $this->repo->applyFilter($filter);

        $request=request();
        if ($request->ajax()) {
            return DataTables::of($data)
                ->editColumn('title', function($data)
                {
                   return $data->translations
                   ->where('language_id',session('language'))
                   ->first()
                   ->title;
                })
                ->editColumn('description', function($data)
                {
                   return $data->translations
                   ->where('language_id',session('language'))
                   ->first()
                   ->description;
                })
                ->editColumn('category', function($data)
                {
                   return $data->category
                   ->translations
                   ->where('language_id',session('language')
                   )->first()
                   ->title;
                })
                ->editColumn('sub_category', function($data)
                {
                   return $data->subCategory
                   ->translations
                   ->where('language_id',session('language'))
                   ->first()
                   ->title;
                })
                ->editColumn('title_author', function($data)
                {
                   return $data->titleAuthor->name;
                })
                ->editColumn('description_author', function($data)
                {
                   return $data->descriptionAuthor->name;
                })
                ->editColumn('areas', function($data)
                {
                    $storesIds=$data->stores->pluck('id');
                    $areaIds=\DB::table('area_store')->whereIn('store_id',$storesIds)->pluck('area_id');
                    $areaNames=\DB::table('area_translations')->whereIn('area_id',$areaIds)->where('language_id',1)->pluck('name');
                    
                    if(!empty($areaNames)){
                        return implode(",",$areaNames->toArray());
                    }
                    return '';
                })
                ->editColumn('cities', function($data)
                {
                    $storesIds=$data->stores->pluck('id');
                    $cityIds=\DB::table('city_store')->whereIn('store_id',$storesIds)->pluck('city_id');
                    $cityNames=\DB::table('city_translations')->whereIn('city_id',$cityIds)->where('language_id',1)->pluck('name');
                    
                    if(!empty($cityNames)){
                        return implode(",",$cityNames->toArray());
                    }
                    return '';
                })
                ->addIndexColumn()
                ->addColumn('action', 'items.action')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        $page_title = 'Items';
        $page_description = 'This page is to show all the records in items table';

        return view('items.index', compact('page_title', 'page_description','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Items';
        $page_description = 'This page is to create new item';

        return view('items.create', compact('page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {      
                
        $request->validate([
            'title_en'=>'required',
            'description_en'=>'required',
            'title_ar'=>'required',
            'description_ar'=>'required',
            'category_id'=>'required|exists:categories,id',
            'sub_category_id'=>'required|exists:sub_categories,id',
            'title_author_id'=>'required|exists:users,id',
            'des_author_id'=>'required|exists:users,id',
            'price'=>'required',
            'arrival_range'=>'required',
            'in_stock'=>'required'
        ]);      

        $this->repo->save($request);

        return back()->with(['success'=>'New Category has been added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {session(['language' => '2']);
        $entity=$this->repo->fetchOne('id',$id);
        if(!$entity)
            return redirect('/')->with('error','not found');
            
        $className=class_basename($entity);
        $page_title = 'Show '.$className;
        $page_description = "This page is to show $className details";
        
        return view(Str::plural($className).'.show',compact('entity','className', 'page_title', 'page_description'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity=$this->repo->fetchOne('id',$id);
        if(!$entity)
            return redirect('/')->with('error','not found');
            
        $className=class_basename($entity);
        $page_title = 'Show '.$className;
        $page_description = "This page is to edit $className details";
        
        return view(Str::plural($className).'.show',compact('entity','className', 'page_title', 'page_description'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->repo->update($id,$request);

        return back()->with('success','Category has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);

        return response()->json([
            'message'=>'success'
        ]);
    }
}
