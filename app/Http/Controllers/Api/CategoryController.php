<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Repositories\CategoriesRepository;
use App\Http\Requests\CategoryStoreRequest;

class CategoryController extends ApiBaseController
{
    protected $repo;

    public function __construct(CategoriesRepository $categoriesRepo)
    {
        $this->repo=$categoriesRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(
            $this->repo->fetchAll(false,true)
                ->toJson(JSON_PRETTY_PRINT)
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CategoryStoreRequest $request)
    {
        $image_path='';

        if($request->has('image')){
            $imageName = time().'.'.$request->image->extension();  
     
            $image_path= $request->image->move(storage_path('images/category/images'), $imageName);
        }

        $this->repo->save($request->all() + ['image_url' => $image_path]);

       return  response()->json([
           'success'=>'Category has been created.'
       ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json([
            $this->repo->fetchOne('id',$id)
        ]);
    }
}
