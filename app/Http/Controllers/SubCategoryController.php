<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\SubCategoriesRepository;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class SubCategoryController extends Controller
{

    protected $repo;

    public function __construct(SubCategoriesRepository $subCategoriesRepo)
    {
        $this->repo=$subCategoriesRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($categoryId)
    {
        
        $data = $this->repo->fetchAllByCategory($categoryId);

        $request=request();
        if ($request->ajax()) {
            return $this->dataTable($data);
        }

        $page_title = 'Sub Categories';
        $page_description = 'This page is to show all the records in country table';

        return view('sub_categories.index', compact('categoryId','page_title', 'page_description','data'));
    }

    protected function dataTable($data)
    {
        return DataTables::of($data)
                ->editColumn('title_en', function($data)
                {
                   return $data->translations->where('language_id',1)->first()->title;
                })
                ->editColumn('title_ar', function($data)
                {
                   return $data->translations->where('language_id',2)->first()->title;
                })
                ->addIndexColumn()
                ->addColumn('action', 'sub_categories.action')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
    }

    public function indexAll()
    {
        $data = $this->repo->fetchAll();

        $request=request();
        if ($request->ajax()) {
            return $this->dataTable($data);
        }

        $page_title = 'Sub Categories';
        $page_description = 'This page is to show all the records in country table';

        return view('sub_categories.index', compact('categoryId','page_title', 'page_description','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($categoryId)
    {
        $page_title = 'Categories';
        $page_description = 'This page is to create new category';

        return view('sub_categories.create', compact('page_title', 'page_description','categoryId'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,$categoryId)
    {        
        $request->validate([
            'title_en'=>'required_without:title_ar',
            'title_ar'=>'required_without:title_en',
            'image'=>'required|image|mimes:png,jpeg,gif'
        ]);

        $this->repo->save($request);

        return back()->with(['success'=>'New Category has been added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($categoryId,$subCategoryId)
    {
        $entity=$this->repo->fetchOne('id',$subCategoryId);

        if(!$entity)
            return redirect('/')->with('error','not found');
            
        $className='Sub Category';
        $page_title = 'Show '.$className;
        $page_description = "This page is to show $className details";
        
        return view('sub_categories.show',compact('entity','categoryId','className', 'page_title', 'page_description'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$categoryId,$subCategoryId)
    {
        $this->repo->update($subCategoryId,$request);

        return back()->with('success','Category has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($categoryId,$subCategoryId)
    {
        $this->repo->delete($subCategoryId);

        return response()->json([
            'message'=>'success'
        ]);
    }
}
