<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\CategoriesRepository;
use Yajra\DataTables\Facades\DataTables;
use Illuminate\Support\Str;

class CategoryController extends Controller
{

    protected $repo;

    public function __construct(CategoriesRepository $categoriesRepo)
    {
        $this->repo=$categoriesRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $data = $this->repo->fetchAll();

        $request=request();
        if ($request->ajax()) {
            return DataTables::of($data)
                ->editColumn('title_en', function($data)
                {
                   return $data->translations->where('language_id',1)->first()->title;
                })
                ->editColumn('title_ar', function($data)
                {
                   return $data->translations->where('language_id',2)->first()->title;
                })
                ->addIndexColumn()
                ->addColumn('action', 'categories.action')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        $page_title = 'Categories';
        $page_description = 'This page is to show all the records in country table';

        return view('categories.index', compact('page_title', 'page_description','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Categories';
        $page_description = 'This page is to create new category';

        return view('categories.create', compact('page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
        $request->validate([
            'title_en'=>'required_without:title_ar',
            'title_ar'=>'required_without:title_en',
            'image'=>'required|image|mimes:png,jpeg,gif'
        ]);

        $this->repo->save($request->all()+['title_author_id'=>1]);

        return back()->with(['success'=>'New Category has been added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entity=$this->repo->fetchOne('id',$id);
        if(!$entity)
            return redirect('/')->with('error','not found');
            
        $className=class_basename($entity);
        $page_title = 'Show '.$className;
        $page_description = "This page is to show $className details";
        
        return view(Str::plural($className).'.show',compact('entity','className', 'page_title', 'page_description'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity=$this->repo->fetchOne('id',$id);
        if(!$entity)
            return redirect('/')->with('error','not found');
            
        $className=class_basename($entity);
        $page_title = 'Show '.$className;
        $page_description = "This page is to edit $className details";
        
        return view(Str::plural($className).'.show',compact('entity','className', 'page_title', 'page_description'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->repo->update($id,$request);

        return back()->with('success','Category has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);

        return response()->json([
            'message'=>'success'
        ]);
    }
}
