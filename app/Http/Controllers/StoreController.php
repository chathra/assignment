<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Repositories\StoresRepository;
use Yajra\DataTables\Facades\DataTables;
use App\Traits\HasLanguages;
use Illuminate\Support\Str;

class StoreController extends Controller
{
    use HasLanguages;

    protected $repo;

    public function __construct(StoresRepository $storesRepo) 
    {
        $this->repo=$storesRepo;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session(['language' => 2]);
        
        $data = $this->repo->fetchAll();

        $request=request();
        if ($request->ajax()) {
            return DataTables::of($data)
                ->editColumn('title', function($data)
                {
                   return $data->translations
                   ->where('language_id',session('language'))
                   ->first()
                   ->title;
                })
                ->editColumn('location', function($data)
                {
                   return $data->translations
                   ->where('language_id',session('language'))
                   ->first()
                   ->location_text;
                })
                ->editColumn('active_status', function($data)
                {
                   return $data->is_active?'Active':'inactive';
                })             
                ->addIndexColumn()
                ->addColumn('action', 'stores.action')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        $pageTitle = 'Stores';
        $pageDescription = 'This page is to show all the records in stores table';

        return view('stores.index', compact('pageTitle', 'pageDescription','data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $page_title = 'Store';
        $page_description = 'This page is to create new item';

        return view('stores.create', compact('page_title', 'page_description'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {      
                
        $request->validate([
            'title_en'=>'required',
            'title_en'=>'required',
            'location_en'=>'required',
            'location_ar'=>'required',
            'phone'=>'required',
            'iamge'=>'image',
            'cover'=>'image',
        ]);      

        $this->repo->save($request);

        return back()->with(['success'=>'New Category has been added']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $entity=$this->repo->fetchOne('id',$id);
        if(!$entity)
            return redirect('/')->with('error','not found');
            
        $className=class_basename($entity);
        $page_title = 'Show '.$className;
        $page_description = "This page is to show $className details";
        
        return view(Str::plural($className).'.show',compact('entity','className', 'page_title', 'page_description'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $entity=$this->repo->fetchOne('id',$id);
        if(!$entity)
            return redirect('/')->with('error','not found');
            
        $className=class_basename($entity);
        $page_title = 'Show '.$className;
        $page_description = "This page is to edit $className details";
        
        return view(Str::plural($className).'.show',compact('entity','className', 'page_title', 'page_description'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $this->repo->update($id,$request);

        return back()->with('success','Category has been updated.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->repo->delete($id);

        return response()->json([
            'message'=>'success'
        ]);
    }
}
