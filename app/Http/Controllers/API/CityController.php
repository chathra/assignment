<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Repositories\CitiesRepository;

class CityController extends ApiBaseController
{
    protected $repo;

    public function __construct(CitiesRepository $citiesRepo)
    {
        $this->repo=$citiesRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(
            $this->repo->fetchAll(false,true)
                ->toJson(JSON_PRETTY_PRINT)
        );
    }
}
