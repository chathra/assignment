<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Repositories\AreasRepository;

class AreaController extends ApiBaseController
{
    protected $repo;

    public function __construct(AreasRepository $areasRepo)
    {
        $this->repo=$areasRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(
            $this->repo->fetchAll(false,true)
                ->toJson(JSON_PRETTY_PRINT)
        );
    }
}
