<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Repositories\StoresRepository;

class StoreController extends ApiBaseController
{
    protected $repo;

    public function __construct(StoresRepository $storesRepo)
    {
        $this->repo=$storesRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(
            $this->repo->fetchAll(false,true)
                ->toJson(JSON_PRETTY_PRINT)
        );
    }
}
