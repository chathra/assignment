<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Repositories\SubCategoriesRepository;

class SubCategoryController extends ApiBaseController
{
    protected $repo;

    public function __construct(SubCategoriesRepository $subCategoriesRepo)
    {
        $this->repo=$subCategoriesRepo;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response(
            $this->repo->fetchAll(false,true)
                ->toJson(JSON_PRETTY_PRINT)
        );
    }

    public function selectByCategory($categoryId)
    {
        return response(
            $this->repo->fetchAll(false,true)->where('category_id',$categoryId)
                ->toJson(JSON_PRETTY_PRINT)
        );
    }
}
