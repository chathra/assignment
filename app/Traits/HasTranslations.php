<?php

namespace App\Traits;

trait hasTranslations 
{

    public function translations()
    {
        $class=get_class($this)."Translation";
        return $this->hasMany(new $class);
    }
}