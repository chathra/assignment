<?php

namespace App\Traits;

use App\Models\Language;

trait HasLanguages 
{
    protected $en_id;
    protected $ar_id;

    public function __construct()
    {
        $this->en_id=$this->getLanguageIdByIso('en');
        $this->en_id=$this->getLanguageIdByIso('ar');
    }

    protected function getLanguageIdByIso($languageIso)
    {
        return Language::where('iso',$languageIso)->first()->id;
    }
}